from Team import Team
from Ground import Ground
from Toss import Toss
from Inning import Inning
from typing import List
from dal.MatchDAL import MatchDAL
from loguru import logger
from Constants import Match_Type
from Constants import Play_Type
class Match:
	def __init__(self, team1: Team = None, team2: Team = None, match_type: str = None, ground: Ground = None):
		self.id: int = None
		self.team1: Team = team1
		self.team2: Team = team2
		self.match_type: str = match_type
		self.ground: Ground = ground
		self.toss: Toss = None
		self.innings: List[Inning] = None
		self.result: int = -1 		# teamid of the winning team, -1 if abandoned, -2 if draw.
	
	def queue_match(self):
		self.toss = Toss(self.team1.team_id, self.team2.team_id)
		self.toss.toss()
		self.id = self.register_match()
		return self.id
	
	def simulate2(self, id):
		self.id = id
		row = MatchDAL.get_match_info(self.id)
		row = dict(row)
		self.team1 = Team.get_team_by_id(int(row['team1_id']))
		self.team2 = Team.get_team_by_id(int(row['team2_id']))
		self.match_type = row['match_type']
		self.ground = Ground.get_ground_by_id(int(row['ground_id']))
		self.toss = Toss(self.team1.team_id, self.team2.team_id, int(row['toss_winner']), row['toss_decision'])
		
		batting_team: Team = None
		bowling_team: Team = None

		if(self.toss.toss_winner == self.team1.team_id):
			if(self.toss.toss_decision == Play_Type.BAT):
				batting_team = self.team1
				bowling_team = self.team2
			else:
				batting_team = self.team2
				bowling_team = self.team1
		else:
			if(self.toss.toss_decision == Play_Type.BAT):
				batting_team = self.team2
				bowling_team = self.team1
			else:
				batting_team = self.team1
				bowling_team = self.team2

		first_inning: Inning = Inning()
		
		first_inning.set_match(self).set_match_type(self.match_type).set_batting_team(batting_team) \
			.set_bowling_team(bowling_team) \
			.set_max_overs(Match_Type.MAX_OVERS[self.match_type]).set_inning(1).set_target(10000)
		first_inning.simulate()
		
		temp = batting_team
		batting_team = bowling_team
		bowling_team = temp
		second_inning: Inning = Inning()
		second_inning.set_match(self).set_match_type(self.match_type).set_batting_team(batting_team) \
			.set_bowling_team(bowling_team) \
			.set_max_overs(Match_Type.MAX_OVERS[self.match_type]).set_inning(2).set_target(first_inning.current_score)
		second_inning.simulate()

		if(second_inning.current_score > first_inning.current_score):
			self.result = second_inning.batting_team.team_name
			print(f"{second_inning.batting_team.team_name} won by {10 - second_inning.current_wickets} wickets.")
		elif(second_inning.current_score == first_inning.current_score):
			self.result = -2
			print("Match drawn!")
		elif(second_inning.current_score < first_inning.current_score):
			self.result = first_inning.batting_team.team_name
			print(f"{first_inning.batting_team.team_name} won by {second_inning.target - second_inning.current_score + 1} runs.")
		
		self.save_match(first_inning, second_inning)
		MatchDAL.finish_match(self.id)

	def simulate(self):
		#Simulate the toss. Results in one team winning it and deciding whether to bowl/bat.
		self.toss = Toss(self.team1.team_id, self.team2.team_id)
		self.toss.toss()
		self.id = self.register_match()
		if(self.id is None):
			logger.error("Could not register match. Aborting")
			return None
		batting_team: Team = None
		bowling_team: Team = None
		
		if(self.toss.toss_winner == self.team1.team_id):
			if(self.toss.toss_decision == Play_Type.BAT):
				batting_team = self.team1
				bowling_team = self.team2
			else:
				batting_team = self.team2
				bowling_team = self.team1
		else:
			if(self.toss.toss_decision == Play_Type.BAT):
				batting_team = self.team2
				bowling_team = self.team1
			else:
				batting_team = self.team1
				bowling_team = self.team2
		
		first_inning: Inning = Inning()
		
		first_inning.set_match(self).set_match_type(self.match_type).set_batting_team(batting_team) \
			.set_bowling_team(bowling_team) \
			.set_max_overs(Match_Type.MAX_OVERS[self.match_type]).set_inning(1).set_target(10000)
		first_inning.simulate()
		
		temp = batting_team
		batting_team = bowling_team
		bowling_team = temp
		second_inning: Inning = Inning()
		second_inning.set_match(self).set_match_type(self.match_type).set_batting_team(batting_team) \
			.set_bowling_team(bowling_team) \
			.set_max_overs(Match_Type.MAX_OVERS[self.match_type]).set_inning(2).set_target(first_inning.current_score)
		second_inning.simulate()

		if(second_inning.current_score > first_inning.current_score):
			self.result = second_inning.batting_team.team_name
			print(f"{second_inning.batting_team.team_name} won by {10 - second_inning.current_wickets} wickets.")
		elif(second_inning.current_score == first_inning.current_score):
			self.result = -2
			print("Match drawn!")
		elif(second_inning.current_score < first_inning.current_score):
			self.result = first_inning.batting_team.team_name
			print(f"{first_inning.batting_team.team_name} won by {second_inning.target - second_inning.current_score + 1} runs.")
		self.save_match(first_inning, second_inning)

	def register_match(self):
		return MatchDAL.register_match(self)

	def save_match(self, first_inning, second_inning):
		MatchDAL.save_match(first_inning.as_list())
		MatchDAL.save_match(second_inning.as_list())

	@staticmethod
	def get_match_data(match_id):
		row = MatchDAL.get_match_info(match_id)
		row = dict(row)
		match_info = {
			'id': row['id'],
			'team1_id': row['team1_id'],
			'team2_id': row['team2_id'],
			'match_type': row['match_type'],
			'ground_id': row['ground_id'],
			'toss_winner': row['toss_winner'],
			'toss_decision': row['toss_decision'],
			'match_result': row['match_result'],
			'finished': row['finished'],
			'processing': row['processing']
		}
		first_inn = {
			'inning': 1,
			'inning_data': Match.as_dict(match_id, 1)
		}
		second_inn = {
			'inning': 2,
			'inning_data': Match.as_dict(match_id, 2)
		}
		match_info['match_data'] = [first_inn, second_inn]
		return match_info

	@staticmethod
	def as_dict(match_id, inn):	
		rows = MatchDAL.get_match_data(match_id, inn)
		inning_data = []
		for row in rows:
			#print(row)
			row = dict(row)
			ball_data = {
				'match_id': row['match_id'],
				'batting_team': row['batting_team'],
				'bowling_team': row['bowling_team'],
				'inning': row['inning'],
				'bowler': row['bowler'],
				'on_strike': row['on_strike'],
				'runner_end': row['runner_end'],
				'ball_count': row['ball_count'],
				'bowling_outcome': row['bowling_outcome'],
				'batting_outcome': row['batting_outcome'],
				'outcome_desc': row['outcome_desc'],
				'dismissal_type': row['dismissal_type'],
				'dismissed_batsman': row['dismissed_batsman'],
				'fielder': row['fielder'],
				'is_wicketkeeper': row['is_wicketkeeper'],
				'commentary': row['commentary'],
				'is_wicket': row['is_wicket']
			}
			inning_data.append(ball_data)
		return inning_data

	def __str__(self):
	 return "Match [id: {}, team1: {}, team2: {}, match_type: {}, ground: {}]\n{}".format(self.id, self.team1.team_id, self.team2.team_id, self.match_type, self.ground.id, self.toss)