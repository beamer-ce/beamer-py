create table team_info (
	id bigserial primary key,
	name varchar(256) not null
);

alter table team_info add column shortform varchar(8);

create table team_data (
	team_id bigint not null,
	player_id bigint not null,
	constraint team_id_fk foreign key (team_id) references team_info(id) on delete cascade
);

alter table team_data add column player_role varchar(128);
alter table team_data add column position int;

-- match_result == id of the winning team, -1 if abandoned -2 if tie
create table match_info (
	id bigserial primary key,
	team1_id bigint not null,
	team2_id bigint not null,
	match_type varchar(8) not null,
	ground_id bigint,
	toss_winner bigint not null,
	toss_decision varchar(8) not null,
	match_result bigint not null,
	constraint team1_id_fk foreign key (team1_id) references team_info(id) on delete cascade,
	constraint team2_id_fk foreign key (team2_id) references team_info(id) on delete cascade
);

alter table match_info add column finished int default 0;
alter table match_info add column processing int default 0;


-- inning can be 1, 2, 3, 4
-- bowler, batsman, runner_end, fielder -- id
-- ball_count current ball count
-- outcome 0, 1, 2, 3, 4, 6, W, Wide, Noball
-- outcome_desc -- ???
-- dismissal_type -- bowled, runout, stump, lbw etc.
-- commentary -- unused for now
create table match_data (
	match_id bigint not null,
	batting_team bigint not null,
	bowling_team bigint not null,
	inning smallint not null,
	
	bowler bigint not null,
	on_strike bigint not null, 
	runner_end bigint not null,
	ball_count bigint not null,
	bowling_outcome varchar(128) not null,
	batting_outcome varchar(128) not null,
	outcome_desc varchar(128),
	dismissal_type varchar(128),
	dismissed_batsman bigint,
	fielder bigint, 
	is_wicketkeeper smallint,
	commentary text,

	constraint match_id_fk foreign key (match_id) references match_info (id) on delete cascade,
	constraint bowl_team_id_fk foreign key (batting_team) references team_info (id) on delete cascade,
	constraint bat_team_id_fk foreign key (batting_team) references team_info (id) on delete cascade
);

alter table match_data add column is_wicket smallint default 0;