from Player import Player
class Ball:
	def __init__(self):
		self.bowler: Player = None			#Player
		self.on_strike: Player = None 		#Player
		self.runner_end: Player = None		#Player

		self.ball_count = None
		self.commentary = None		#String
		self.over_text = None

		self.current_score = None
		self.current_wickets = None

		self.bowling_outcome = None
		self.batting_outcome = None
		self.outcome_desc = None

		self.is_wicket = None		#boolean ### IMPORTANT
		self.is_free_hit = False		#boolean

		self.outcome = None		#Deprecated?
		self.wicket_details = None	#Wicket_Details
		
	
	def set_ball_count(self, ball_count):
		self.ball_count = ball_count
		return self
	
	def set_over(self, over):
		self.over = over
		return self
	
	def set_outcome(self, outcome):
		self.outcome = outcome
		return self
	
	def set_on_strike(self, on_strike):
		self.on_strike = on_strike
		return self
	
	def set_runner_end(self, runner_end):
		self.runner_end = runner_end
		return self
	
	def set_bowler(self, bowler):
		self.bowler = bowler
		return self
	
	def set_is_wicket(self, is_wicket):
		self.is_wicket = is_wicket
		return self
		
	def set_wicket_details(self, wicket_details):
		self.wicket_details = wicket_details
		return self
	
	def set_commentary(self, commentary):
		self.commentary = commentary
		return self

	def set_batting_outcome(self, batting_outcome):
		self.batting_outcome = batting_outcome
		return self
	
	def set_bowling_outcome(self, bowling_outcome):
		self.bowling_outcome = bowling_outcome
		return self

	def set_is_free_hit(self, is_free_hit):
		self.is_free_hit = is_free_hit
		return self
	
	def set_over_text(self, over_text):
		self.over_text = over_text
		return self

	def set_current_score(self, current_score):
		self.current_score = current_score
		return self

	def set_current_wickets(self, current_wickets):
		self.current_wickets = current_wickets
		return self

	def set_outcome_desc(self, outcome_desc):
		self.outcome_desc = outcome_desc
		return self

	def __str__(self):
		return """ball: {}, bowl_oc: {}, bat_oc: {}, on_strike: {}, runner_end: {}, bowler: {}, is_wicket: {}, is_free_hit: {}, """ \
		.format(self.over_text, self.bowling_outcome \
			,self.batting_outcome, self.on_strike.name, self.runner_end.name, \
				self.bowler.name, self.is_wicket, self.is_free_hit)

class Wicket_Details:
	def __init__(self, dismissal_type, batsman, bowler, fielder, is_wicketkeeper):
		self.dismissal_type = dismissal_type
		self.batsman: Player = batsman
		self.bowler: Player = bowler
		self.fielder: Player = fielder
		self.is_wicketkeeper = is_wicketkeeper