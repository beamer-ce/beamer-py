from dal.GroundDAL import GroundDAL
class Ground:
	def __init__(self, id, ci_id, name, country):
		self.id = id
		self.ci_id = ci_id
		self.name = name
		self.country = country
	
	def __str__(self):
	 return "Ground [id: {}, ci_id: {}, name: {}, country: {}]".format(self.id, self.ci_id, self.name, self.country)

	@staticmethod
	def get_ground_by_id(id):
		if id is None:
			return None
		row = GroundDAL.get_ground_by_id(id)
		return Ground.deserialize(row)
	
	@staticmethod
	def deserialize(row):
		row = dict(row)
		ground = Ground(row['id'], row['ci_id'], row['name'], row['country'])
		return ground
	
	@staticmethod
	def as_dict(id):
		stadium_data = []
		if(id == "all"):
			rows = GroundDAL.get_all_grounds()
			for row in rows:
				row = dict(row)
				ground_data = {
					'id': row['id'],
					'name': row['name'],
					'country': row['country']
				}
				stadium_data.append(ground_data)
		else:
			row = GroundDAL.get_ground_by_id(id)
			row = dict(row)
			ground_data = {
				'id': row['id'],
				'name': row['name'],
				'country': row['country']
			}
			stadium_data.append(ground_data)
		return stadium_data

