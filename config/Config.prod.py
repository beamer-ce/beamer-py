class PG_Database_Config:
	host = "localhost"
	port = 5432
	database = "beamer"
	user = "beamer"

class SL_Database_Config:
	db_path = "/home/nbn/deploy/data/data.sqlite"

class Config:
	ce_log_path = "logs/ce/application.log"
	api_log_path = "logs/api/applcation.log"
	span_length = 10
	debug = False
	live_reload = False