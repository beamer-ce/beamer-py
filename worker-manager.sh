#!/usr/bin/env bash
# echo $1 $2
command=$1
num=$2

if [ "$command" == "add" ]
then
	for ((i=0; i <$2; i++))
	do
		nohup python3 worker.py &
		echo $! >> PROCESS_LIST
	done
fi

if [ "$command" == "remove" ]
then
	head -n $num PROCESS_LIST | xargs -n 1 kill
	sed -i -e "1,$num d" PROCESS_LIST
fi

if [ "$command" == "list" ]
then 
	if [ ! -f PROCESS_LIST ] 
	then 
		echo "No process active"
	else
		cat PROCESS_LIST
	fi
fi

if [ "$command" == "kill" ]
then 
	kill $num
	sed -i "/$num/d" PROCESS_LIST
fi

if [ "$command" == "killall" ]
then 
	if [ ! -f PROCESS_LIST ] 
	then 
		echo "No process active"
	else
		cat PROCESS_LIST | xargs -n 1 kill 
		rm PROCESS_LIST
		touch PROCESS_LIST
	fi
fi

if [ "$command" == "help" ]
then 
	echo "help -- Print this text."
	echo "add <num> -- Spawn <num> processes."
	echo "remove <num> -- Remove <num> processes."
	echo "list -- List all active processes."
	echo "kill <pid> -- Kill process by <pid>."
	echo "killall -- Kill all active processes."
fi

