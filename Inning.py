from Team import Team
from Ball import Ball
from Player import Player
from typing import List
from Over import Over
from Machine import BowlerSelector
from Constants import Match_Type
from dal.MachineDAL import MachineDAL
class Inning:
	""" Contains the inning data for a match. A inning constitutes of a complete batting/bowling session.
	"""
	def __init__(self, batting_team: Team = None, bowling_team: Team = None, max_overs: int = None, inning:int = None, target: int = None):
		self.match = None
		self.match_type = None
		self.max_overs:int = max_overs
		self.inning:int = inning
		self.target:int = target
		self.batting_team:Team = batting_team
		self.bowling_team:Team = bowling_team
		self.inning_data: List[Ball] = []			# []Ball
		
		self.current_ball: int = 0
		self.current_over: int = 0				
		self.current_score: int = 0
		self.current_wickets: int = 0
		self.current_bowler: Player = None
		self.on_strike: Player = None
		self.runner_end: Player = None

		#not required?
		self.is_prev_no_ball = False	#boolean

	def set_match(self, match):
		self.match = match
		return self

	def set_match_type(self, match_type):
		self.match_type = match_type
		return self

	def set_max_overs(self, max_overs):
		self.max_overs = max_overs
		return self

	def set_inning(self, inning):
		self.inning = inning
		return self
	
	def set_target(self, target):
		self.target = target
		return self
	
	def set_batting_team(self, batting_team):
		self.batting_team = batting_team
		return self
	
	def set_bowling_team(self, bowling_team):
		self.bowling_team = bowling_team
		return self
	
	def add_new_ball(self, ball):
		if(self.inning_data is None):
			self.inning_data = []
		self.inning_data.append(ball)
	
	def set_is_prev_no_ball(self, is_prev_no_ball):
		self.is_prev_no_ball = is_prev_no_ball
		return self
	
	def simulate(self):
		self.on_strike = self.batting_team.player_list[0]
		self.runner_end = self.batting_team.player_list[1]
		#self.current_bowler = self.bowling_team.get_random_bowler()
		self.current_bowler = BowlerSelector.get_next_bowler(self, None, 1, Match_Type.ODI)
		while(self.current_ball < 300):
			#get the batting and bowling outcome from db in the beginning of the over.
			if(self.on_strike is not None):
				self.on_strike.batting_outcome_map = MachineDAL.get_batting_p(self.on_strike, self.current_over, Match_Type.ODI)
			if(self.runner_end is not None):
				self.runner_end.batting_outcome_map = MachineDAL.get_batting_p(self.on_strike, self.current_over, Match_Type.ODI)
			if(self.current_bowler is not None):
				self.current_bowler.bowling_outcome_map = MachineDAL.get_bowling_p(self.current_bowler, self.current_over, Match_Type.ODI)

			cont = Over.simulate(self)
			self.current_over += 1
			self.current_bowler.overs_bowled += 1
			if(cont == 0):
				pass
				print("""Score: {}/{}""" \
				.format(self.current_score, self.current_wickets))
			else:
				print("""Over: {} Score: {}/{} | *{}, {} | {}""" \
				.format(self.current_over, self.current_score, self.current_wickets, \
					self.on_strike.name, \
					self.runner_end.name, \
					self.current_bowler.name))
				temp = self.on_strike
				self.on_strike = self.runner_end
				self.runner_end = temp
			if(cont == 0):
				break
			#self.current_bowler = self.bowling_team.get_random_bowler()
			self.current_bowler = BowlerSelector \
				.get_next_bowler(self, self.current_bowler, self.current_over+1, Match_Type.ODI)

	def __str__(self):
		return "Inning: [inning: {}, batting: {}, bowling: {}, max_overs: {}]".format(self.inning, self.batting_team.team_id, self.bowling_team.team_id, self.max_overs)

	def as_list(self):
		""" Returns the ball data in a list format
		"""
		inning_data = []
		for ball in self.inning_data:
			ball_data = None
			if(ball.is_wicket):
				ball_data = (self.match.id, \
					self.batting_team.team_id, self.bowling_team.team_id, self.inning, \
					ball.bowler.id, ball.on_strike.id, ball.runner_end.id, ball.ball_count, \
					ball.bowling_outcome, ball.batting_outcome, ball.outcome_desc, \
					ball.wicket_details.dismissal_type, ball.wicket_details.batsman.id, \
					ball.wicket_details.fielder.id if ball.wicket_details.fielder is not None else None, \
					1 if ball.wicket_details.is_wicketkeeper else 0, \
					1 if ball.is_wicket else 0, ball.commentary)
			else:
				ball_data = (self.match.id, \
					self.batting_team.team_id, self.bowling_team.team_id, self.inning, \
					ball.bowler.id, ball.on_strike.id, ball.runner_end.id, ball.ball_count, \
					ball.bowling_outcome, ball.batting_outcome, ball.outcome_desc, \
					None, None, \
					None, None, 0, ball.commentary)
			inning_data.append(ball_data)
		return inning_data