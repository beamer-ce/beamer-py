class Player_Type:
	BATSMAN = "batsman"
	BOWLER = "bowler"
	ALLROUNDER = "allrounder"
	WICKETKEEPER = "wicketkeeper"
	CAPTAIN = "captain"					#Not an actual player_type, just added for non-duplicity. 

class Dismissal_Type:
	CAUGHT = "caught"
	RUNOUT = "runout"
	BOWLED = "bowled"
	STUMPED = "stumped"
	LEG_BEFORE_WICKET = "leg before wicket"
	HIT_WICKET = "hit wicket"
	OBSTRUCTING_THE_FIELD = "obstructing the field"
	HANDLED_THE_BALL = "handled the ball"

class Batting_Outcome:
	RUNS = "runs"
	OUT = "out"
	

class Bowling_Outcome:
	GREAT = "great"
	GOOD = "good"
	OK = "ok"
	BAD = "bad"
	WIDE = "wide"
	NO_BALL = "no_ball"

class Play_Type:
	BAT = "bat"
	BOWL = "bowl"

class Match_Type:
	ODI = "odi"
	T20 = "t20"
	TEST = "test"
	MAX_OVERS = {
		"odi": 50,
		"t20": 20,
		"test": -1
	}
	MAX_OVERS_PER_BOWLER = {
		"odi": 10,
		"t20": 4
	}		

class P_Mod:
	LC = 1
	MC = 0.5
	SC = 0

class BOWLER_TYPE: 
	FAST = ["Right-arm fast", "Right-arm fast-medium", "Right-arm medium-fast", "Left-arm fast-medium", \
		"Left-arm fast", "Left-arm medium-fast"]
	MEDIUM = ["Right-arm medium", "Left-arm medium", "Right-arm bowler", "Right-arm slow-medium"]
	SLOW = ["Right-arm offbreak", "Slow left-arm chinaman", "Legbreak googly", "Slow left-arm orthodox", \
		"Legbreak", "Right-arm slow", "Left-arm slow"]
