import psycopg2
from config.Config import PG_Database_Config, SL_Database_Config
from loguru import logger
import os
import sqlite3
class Database:
	# pg_connection: Returns a postgres connection. `None` if some error occurs.
	@staticmethod
	def pg_connection():
		connection = None
		password = os.getenv('PG_PASSWORD') # Don't store your passwords in config files kids ;)
		try:
			connection = psycopg2.connect(
				host = PG_Database_Config.host,
				port = PG_Database_Config.port,
				database = PG_Database_Config.database, 
				user = PG_Database_Config.user,
				password = password
				)
			return connection
		except psycopg2.DatabaseError as e:
			logger.error('Error while acquiring Postgres connection')
			logger.error('{}', e)
			return connection

	# sl_connection: Returns a Sqlite connection. Else `None`
	@staticmethod
	def sl_connection():
		connection = None
		try:
			connection = sqlite3.connect(SL_Database_Config.db_path)
			connection.row_factory = sqlite3.Row
			return connection
		except sqlite3.DatabaseError as e:
			logger.error('Error while acquiring Sqlite connection.')
			logger.error('{}', e)
			return connection
	
	@staticmethod
	def sl_connection_for_write():
		connection = None
		try:
			connection = sqlite3.connect(SL_Database_Config.db_path)
			return connection
		except sqlite3.DatabaseError as e:
			logger.error('Error while acquiring Sqlite connection.')
			logger.error('{}', e)
			return connection