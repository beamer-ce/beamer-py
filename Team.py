from dal.TeamDAL import TeamDAL
from Player import Player
from Constants import Player_Type
from random import randint
class Team:
	""" Team -- Model for the Team entity. Contains the team id, name and the players.
	"""
	def __init__(self, team_id, team_name, player_list):
		self.team_id = team_id
		self.team_name = team_name
		self.player_list = player_list
		self.shortform = None
		self.opening_bowlers = []
	
	def as_dict(self):
		team = {}
		team['team_id'] = self.team_id
		team['team_name'] = self.team_name
		team['shortform'] = self.shortform
		team['player_list'] = []
		for p in self.player_list:
			team['player_list'].append(p.as_dict())
		return team

	#Stub
	#Fetch the players data from db based on the players ci_id
	def add_player(self, player_list):
		pass
	
	def get_captain(self):
		for p in self.player_list:
			if(p.player_role == Player_Type.CAPTAIN):
				return p
		return None
	
	def get_wicketkeeper(self):
		for p in self.player_list:
			if(p.player_role == Player_Type.WICKETKEEPER):
				return p
		return None

	def get_random_bowler(self, exclusion_list = None):
		player = None
		while True:
			player = self.player_list[randint(0, 10)]
			in_exclusion_list = False
			for p in exclusion_list:
				if(p.id == player.id):
					in_exclusion_list = True
			if(in_exclusion_list == False):
				if(player.player_role != Player_Type.WICKETKEEPER):
					return player

	@staticmethod
	def register_team(team):
		TeamDAL.register_team(team)

	@staticmethod
	def get_team_by_id(id):
		""" Fetch the team data based on the id
		Returns:
			`Team`: If data exists. \n
			`None`: If team doesn't exist.
		"""
		rows = TeamDAL.get_team_by_id(id)
		return Team.deserialize(rows)

	@staticmethod
	def deserialize(rows):
		if(len(rows) == 0):
			return None
		new_team = Team(rows[0]['id'], rows[0]['name'], None)
		new_team.shortform = rows[0]['shortform']
		player_list = []
		player_ids = [None] * len(rows)
		for row in rows:
			player_ids[row['position']] = row['player_id']
		new_team.player_list = Player.get_players_by_id(id_list=player_ids)

		for player in new_team.player_list:
			for row in rows:
				if(row['player_id'] == player.id):
					player.set_player_role(row['player_role'])

		return new_team

	@staticmethod
	def get_team_summary():
		rows = TeamDAL.get_team_summary()
		team_list = []
		for row in rows:
			row = dict(row)
			team_info = {
				'id': row['id'],
				'name': row['name'],
				'shortform': row['shortform']
			}
			team_list.append(team_info)
		return team_list


	def __str__(self):
		player_data = ""
		for player in self.player_list:
			player_data += str(player) + "\n"
		return "Team [id: {}, name: {}] \n{}".format(self.team_id, self.team_name, player_data)