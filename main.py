from bottle import route, run, template, Bottle, abort, response, request, HTTPResponse
from utils.DatabaseUtils import Database
import sys
from loguru import logger
from config.Config import Config
from Team import Team
from Match import Match
from Ground import Ground
from truckpad.bottle.cors import CorsPlugin, enable_cors
import redis 

app = Bottle()
logger.remove()
logger.add(Config.api_log_path, rotation="1 Day", colorize=True, compression="zip")
r = redis.Redis(host="localhost", port=6379, db=0, decode_responses= True)

@enable_cors
@app.route('/api/beamer/v1/status')
def get_status():
	status = {
		'code': 200,
		'message': 'OK'
	}
	return status

@enable_cors
@app.get('/api/beamer/v1/team/all/summary')
def get_team_summary():
	team_summary = Team.get_team_summary()
	#add the placeholder team
	team_summary.insert(0, { 'id': -1, 'name': 'Select', 'shortForm': 'N/A'})
	data = {
		'code': 200,
		'data': team_summary
	}
	return data

@enable_cors
@app.get('/api/beamer/v1/team/<id:int>/details')
def get_team_details(id):
	if(not isinstance(id, int)):
		response = HTTPResponse({'status': 400, 'data': 'Invalid team id.'}, 400)
		response.set_header('Content-Type', 'application/json')
		return response
		# return abort(400, 'Invalid team id.')
	team = Team.get_team_by_id(id)
	data = {
		'code': 200,
		'data': team.as_dict()
	}
	return data

@enable_cors
@app.get('/api/beamer/v1/match/<id:int>/live')
def get_match_data(id):
	if(not isinstance(id, int)):
		response = HTTPResponse({'status': 400, 'data': 'Invalid match id.'}, 400)
		response.set_header('Content-Type', 'application/json')
		return response
	match_info = Match.get_match_data(id)
	data = {
		'code': 200,
		'data': match_info
	}
	return data

@enable_cors
@app.get('/api/beamer/v1/stadium/<id>/details')
def get_stadium_data(id):
	stadium_info = Ground.as_dict(id)
	if(id == 'all'):
		stadium_info.insert(0, {'id': -1, 'name': 'Select', 'country': 'None'})
	data = {
		'code': 200,
		'data': stadium_info
	}
	return data

@enable_cors
@app.post('/api/beamer/v1/match/simulate')
def queue_match():
	data = request.json
	team1 = Team.get_team_by_id(data['team1Id'])
	team2 = Team.get_team_by_id(data['team2Id'])
	ground = Ground.get_ground_by_id(data['stadiumId'])
	match = Match(team1, team2, "odi", ground)
	match_id = match.queue_match()
	r.lpush('match_queue', match_id)
	data = {
		'code': 200,
		'data': match_id
	}
	return data

app.install(CorsPlugin(origins=['http://localhost:3030', 'http://localhost:8080']))
run(app, server='cherrypy', host='localhost', port=8080, reloader=Config.live_reload, debug=Config.debug)	