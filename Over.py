#from Inning import Inning
from Player import Player
from Ball import Ball, Wicket_Details
from typing import List
from loguru import logger
from Machine import OutcomeCalculator
from Constants import Bowling_Outcome, Batting_Outcome, Dismissal_Type
from random import randint
import math
import time
from dal.MachineDAL import MachineDAL

#TODO: CLEAN DA SPAGHETTI

class Over:
	""" Contains data for the current over. Has no representation in database.
	!WARNING: Methods of this class modify arguments passed by reference.
	"""
	def __init__(self, inning):
		#shorter name for brevity
		self.inn = inning
		self.current_over: List[Ball] = None

	@staticmethod
	def simulate(inn) -> bool:
		""" Simulates an over of balls (6 valid deliveries).
		It modifies the `inn` property. Each ball bowled modifies `inning_data`,
		`current_ball`, `current_score`, `current_wickets`, `current_bowler`, `on_strike`,
		`runner_end` property of the `inning` object.

		Returns:
			`int`: Integer
				0: Don't bowl the next over, the inning is concluded.
				1: Bowl the next over.
		"""
		valid_ball_count = 0
		while (valid_ball_count < 6):
			# time.sleep(0.1)
			if(Over.has_innings_finished(inn)):
				return 0

			bowling_outcome = OutcomeCalculator.get_bowling_outcome(inn)
			batting_outcome = OutcomeCalculator.get_batting_outcome(inn, bowling_outcome)
			Over.modify_confidence(inn, batting_outcome, bowling_outcome)
			try:
				is_free_hit = inn.inning_data[-1].bowling_outcome == Bowling_Outcome.NO_BALL
			except:
				is_free_hit = False

			if(bowling_outcome not in [Bowling_Outcome.WIDE, Bowling_Outcome.NO_BALL]):
				### HANDLE A VALID BALL
				valid_ball_count += 1
				inn.current_ball += 1
				Over.handle_valid_ball(inn, batting_outcome, bowling_outcome, is_free_hit, valid_ball_count)
			elif (bowling_outcome == Bowling_Outcome.WIDE):
				# HANDLE WIDE
				Over.handle_wide_ball(inn, batting_outcome, bowling_outcome, is_free_hit, valid_ball_count)
			else:
				#HANDLE NO BALL
				Over.handle_noball(inn, batting_outcome, bowling_outcome, is_free_hit, valid_ball_count)
			
			# if(inn.inning_data[-1].batting_outcome == "stumped"):
				# print(inn.inning_data[-1])

		if(Over.has_innings_finished(inn)):
			return 0
		else:
			return 1
			
	@staticmethod 
	def modify_confidence(inn, batting_outcome, bowling_outcome):		
		if(bowling_outcome == Bowling_Outcome.NO_BALL):
			inn.current_bowler.bowling_confidence -= 0.2
		elif(bowling_outcome == Bowling_Outcome.WIDE):
			inn.current_bowler.bowling_confidence -= 0.1

		if(batting_outcome[1] == 1):
			inn.on_strike.batting_confidence += 0.05
			inn.current_bowler.bowling_confidence += 0.05
		elif(batting_outcome[1] in [2, 3]):
			inn.on_strike.batting_confidence += 0.1
		elif(batting_outcome[1] == 4):
			inn.on_strike.batting_confidence += 0.2
			inn.current_bowler.bowling_confidence -= 0.2
		elif(batting_outcome[1] == 6):
			inn.on_strike.batting_confidence += 0.3
			inn.current_bowler.bowling_confidence -= 0.3
		elif(batting_outcome[1] == 0):
			inn.on_strike.batting_confidence += 0.01
			inn.current_bowler.bowling_confidence += 0.05
		elif(batting_outcome[1] == Batting_Outcome.OUT):
			inn.current_bowler.bowling_confidence += 0.3

	@staticmethod
	def handle_valid_ball(inn, batting_outcome, bowling_outcome, is_free_hit, valid_ball_count):
		if(batting_outcome[0] != Batting_Outcome.OUT):
			# HANDLE VALID BALL AND RUN SCORED (0, 1, 2, 3, 4, 6)
			inn.current_score += batting_outcome[1]
			ball_data = Ball().set_bowler(inn.current_bowler).set_on_strike(inn.on_strike) \
				.set_runner_end(inn.runner_end).set_ball_count(inn.current_ball) \
				.set_over_text("{}.{}".format(inn.current_over, valid_ball_count)) \
				.set_current_score(inn.current_score).set_current_wickets(inn.current_wickets) \
				.set_batting_outcome(batting_outcome[1]).set_bowling_outcome(bowling_outcome) \
				.set_outcome_desc(batting_outcome[1]).set_is_wicket(False) \
				.set_is_free_hit(is_free_hit)

			if(batting_outcome[1] in [1, 3]):
				temp = inn.on_strike
				inn.on_strike = inn.runner_end
				inn.runner_end = temp
			inn.inning_data.append(ball_data)
			
		else:
			# HANDLE VALID BALL AND WICKET TAKEN
			# the wicket is taken if this ball was not a freehit/noball
			(ball_data, is_wicket, dismissed) = Over.get_dismissal_details(inn, batting_outcome, bowling_outcome, valid_ball_count, is_free_hit)
			inn.inning_data.append(ball_data)
			if(is_wicket):
				#if the wicket was valid, increment the count of wickets
				inn.current_wickets += 1
				dismissed.is_dismissed = True
				next_batsman = Over.get_next_batsman(inn, dismissed)
				if(inn.on_strike.id == dismissed.id):
					inn.on_strike = next_batsman
				else:
					inn.runner_end = next_batsman
	
	@staticmethod
	def handle_wide_ball(inn, batting_outcome, bowling_outcome, is_free_hit, valid_ball_count):
		if(batting_outcome[0] != Batting_Outcome.OUT):
			# HANDLE WIDE BALL AND RUNS SCORED (0, 1, 2, 3, 4) + 1 extra
			inn.current_score += batting_outcome[1] + 1
			ball_data = Ball().set_bowler(inn.current_bowler).set_on_strike(inn.on_strike) \
				.set_runner_end(inn.runner_end).set_ball_count(inn.current_ball) \
				.set_over_text("{}.{}".format(inn.current_over, valid_ball_count)) \
				.set_current_score(inn.current_score).set_current_wickets(inn.current_wickets) \
				.set_batting_outcome(batting_outcome[1]).set_bowling_outcome(bowling_outcome) \
				.set_outcome_desc("({}){}".format(batting_outcome[1], bowling_outcome)) \
				.set_is_wicket(False).set_is_free_hit(is_free_hit)
			inn.inning_data.append(ball_data)
			if(batting_outcome[1] in [1, 3]):
				temp = inn.on_strike
				inn.on_strike = inn.runner_end
				inn.runner_end = temp
		else:
			# HANDLE WIDE BALL AND WICKET TAKEN
			# the wicket is taken if this ball was not a freehit/noball
			(ball_data, is_wicket, dismissed) = Over.get_dismissal_details(inn, batting_outcome, bowling_outcome, valid_ball_count, is_free_hit)
			inn.inning_data.append(ball_data)
			if(is_wicket):
				#if the wicket was valid, increment the count of wickets
				inn.current_wickets += 1
				dismissed.is_dismissed = True
				next_batsman = Over.get_next_batsman(inn, dismissed)
				if(inn.on_strike.id == dismissed.id):
					inn.on_strike = next_batsman
				else:
					inn.runner_end = next_batsman

	@staticmethod
	def handle_noball(inn, batting_outcome, bowling_outcome, is_free_hit, valid_ball_count):
		if(batting_outcome[0] != Batting_Outcome.OUT):
			# HANDLE NO BALL AND RUNS SCORED (0, 1, 2, 3, 4, 6) + 1 extra
			inn.current_score += batting_outcome[1] + 1
			ball_data = Ball().set_bowler(inn.current_bowler).set_on_strike(inn.on_strike) \
				.set_runner_end(inn.runner_end).set_ball_count(inn.current_ball) \
				.set_over_text("{}.{}".format(inn.current_over, valid_ball_count)) \
				.set_current_score(inn.current_score).set_current_wickets(inn.current_wickets) \
				.set_batting_outcome(batting_outcome[1]).set_bowling_outcome(bowling_outcome) \
				.set_outcome_desc("({}){}".format(batting_outcome[1], bowling_outcome)) \
				.set_is_wicket(False).set_is_free_hit(is_free_hit)
			inn.inning_data.append(ball_data)
			if(batting_outcome[1] in [1, 3]):
				temp = inn.on_strike
				inn.on_strike = inn.runner_end
				inn.runner_end = temp
		else:
			# HANDLE NO BALL AND WICKET TAKEN
			(ball_data, is_wicket, dismissed) = Over.get_dismissal_details(inn, batting_outcome, bowling_outcome, valid_ball_count, is_free_hit)
			inn.inning_data.append(ball_data)
			if(is_wicket):
				#if the wicket was valid, increment the count of wickets
				inn.current_wickets += 1
				dismissed.is_dismissed = True
				next_batsman = Over.get_next_batsman(inn, dismissed)
				if(inn.on_strike.id == dismissed.id):
					inn.on_strike = next_batsman
				else:
					inn.runner_end = next_batsman
			

	@staticmethod
	def get_dismissal_details(inn, batting_outcome, bowling_outcome, valid_ball_count, is_free_hit):
		dismissed = None
		fielder = None
		is_wicketkeeper = False
		
		#If the outcome was wicket, randomly dismiss either batsmen
		if (batting_outcome[1] == Dismissal_Type.RUNOUT):
			if(randint(1, 10) < 5):
				dismissed = inn.on_strike
			else:
				dismissed = inn.runner_end
		else:
			dismissed = inn.on_strike
		#choose a fielder if the dismissal type was runout or caught
		if (batting_outcome[1] == Dismissal_Type.RUNOUT or batting_outcome[1] == Dismissal_Type.CAUGHT):
			fielder = inn.bowling_team.player_list[randint(1, 10)]

		if (batting_outcome[1] == Dismissal_Type.STUMPED):
			fielder = inn.bowling_team.get_wicketkeeper()
			is_wicketkeeper = True

		wicket_details = Wicket_Details(batting_outcome[1], dismissed, inn.current_bowler, fielder, is_wicketkeeper)
		
		is_wicket = True
		if(bowling_outcome == Bowling_Outcome.NO_BALL and (batting_outcome[1] != Dismissal_Type.RUNOUT)):
			is_wicket = False

		if(is_free_hit and (batting_outcome[1] != Dismissal_Type.RUNOUT)):
			is_wicket = False

		ball_data = Ball().set_bowler(inn.current_bowler).set_on_strike(inn.on_strike) \
			.set_runner_end(inn.runner_end).set_ball_count(inn.current_ball) \
			.set_over_text("{}.{}".format(inn.current_over, valid_ball_count)) \
			.set_current_score(inn.current_score).set_current_wickets(inn.current_wickets) \
			.set_bowling_outcome(bowling_outcome).set_batting_outcome(batting_outcome[1]) \
			.set_outcome_desc(batting_outcome[1]).set_is_wicket(is_wicket) \
			.set_is_free_hit(is_free_hit).set_wicket_details(wicket_details)
		return (ball_data, is_wicket, dismissed)

	@staticmethod
	def get_next_batsman(inn, dismissed: Player):
		on_strike = inn.on_strike
		runner_end = inn.runner_end
		next_batsman = None

		for player in inn.batting_team.player_list:
			if player.is_dismissed == False and (player.id != on_strike.id and player.id != runner_end.id):
				next_batsman = player
				break
		if next_batsman is not None:
			next_batsman.batting_outcome_map = MachineDAL.get_batting_p(next_batsman, inn.current_over, inn.match_type)
		return next_batsman

	@staticmethod
	def has_innings_finished(inn):
		if ((inn.inning == 2 and inn.current_score > inn.target) or (inn.current_wickets == 10)):
			return True
		return False



