from loguru import logger
from dal.PlayerDAL import PlayerDAL
from random import randint
class Player:
	def __init__(self):
		self.id = None
		self.ci_id = None
		self.name = None
		self.batting_name = None
		self.fielding_name = None
		self.country = None
		self.player_type = None
		self.batting_style = None
		self.bowling_style = None
		self.player_role = None
		self.is_dismissed = False
		self.overs_bowled = 0
		self.batting_confidence = 0
		self.bowling_confidence = 0

		self.batting_outcome_map = None
		self.bowling_outcome_map = None
	
	def set_id(self, id):
		self.id = id
		return self
	
	def set_ci_id(self, ci_id):
		self.ci_id = ci_id
		return self

	def set_name(self, name):
		self.name = name
		return self

	def set_batting_name(self, batting_name):
		self.batting_name = batting_name
		return self

	def set_fielding_name(self, fielding_name):
		self.fielding_name = fielding_name
		return self
	
	def set_country(self, country):
		self.country = country
		return self
	
	def set_player_type(self, player_type):
		self.player_type = player_type
		return self
	
	def set_batting_style(self, batting_style):
		self.batting_style = batting_style
		return self
	
	def set_bowling_style(self, bowling_style):
		self.bowling_style = bowling_style
		return self

	def set_current_match_details(self, current_match_details):
		self.current_match_details = current_match_details
		return self

	def set_player_role(self, player_role):
		self.player_role = player_role
		return self
	
	def set_overs_bowled(self, overs_bowled):
		self.overs_bowled = overs_bowled
		return self

	def as_dict(self):
		player = {}
		player['id'] = self.id
		# player['ci_id'] = self.ci_id
		player['name'] = self.name
		player['batting_name'] = self.batting_name
		player['fielding_name'] = self.fielding_name
		player['country'] = self.country
		player['player_type'] = self.player_type
		player['batting_style'] = self.batting_style
		player['bowling_style'] = self.bowling_style
		player['player_role'] = self.player_role
		player['is_dismissed'] = self.is_dismissed
		player['overs_bowled'] = self.overs_bowled
		player['batting_confidence'] = self.batting_confidence
		player['bowling_confidence'] = self.bowling_confidence
		return player

	def __str__(self):
	 return "Player [id: {}, ci_id: {}, name: {}, country: {}, player_type: {}, player_role: {}]".format(self.id, self.ci_id, self.name, self.country, self.player_type, self.player_role)

	@staticmethod
	def get_players_by_id(id_list = None, ci_id_list = None):
		if((id_list is not None) and (ci_id_list) is not None):
			logger.error("Cannot pass both id and ci_id in call.")
			return None
		if((id_list is None) and (ci_id_list is None)):
			logger.error("id and ci_id null.")
			return None
		
		if(ci_id_list is not None):
			player_list = [None] * len(ci_id_list)
		else:
			player_list = [None] * len(id_list)
		
		rows = PlayerDAL.get_players_by_id(id_list, ci_id_list)

		if(ci_id_list is not None):
			id_list = ci_id_list

		#map to keep track of what order the player id;s were sent
		player_map = {}
		for index, player_id in enumerate(id_list):
			player_map[player_id] = index

		for row in rows:
			player = Player.deserialize(row)
			if(ci_id_list is not None):
				player_position = player_map[player.ci_id]
			else:
				player_position = player_map[player.id]
			player_list[player_position] = player
		return player_list

	@staticmethod
	def deserialize(row):
		row = dict(row)
		player = Player()
		player.set_id(row['id']).set_ci_id(row['ci_id']) \
		.set_name(row['name']).set_country(row['country']).set_player_type(row['player_type']) \
		.set_batting_style(row['batting_style']).set_bowling_style(row['bowling_style']) \
		.set_batting_name(row['batting_name']).set_fielding_name(row['fielding_name']) 

		return player














#### UNUSED. 
###############################################################################################
class Current_Match_Details:
	def __init__ (self):
		self.batting = Batting()
		self.bowling = Bowling()

class Batting:
	def __init__(self):
		self.runs = 0
		self.balls = 0
		self.fours = 0
		self.sixes = 0
		self.is_dismissed = False
		self.dismissal_details = None #Dismissal_Details
	
	def set_runs(self, runs):
		self.runs = runs
		return self
	
	def set_balls(self, balls):
		self.balls = balls
		return self
	
	def set_fours(self, fours):
		self.fours = fours
		return self
	
	def set_sixes(self, sixes):
		self.sixes = sixes
		return self
	
	def set_is_dismissed(self, is_dismissed):
		self.is_dismissed = is_dismissed
		return self

	def set_dismissal_details(self, dismissal_details):
		self.dismissal_details = dismissal_details
		return self


class Bowling:
	def __init__(self):
		self.overs = 0
		self.balls = 0
		self.runs = 0
		self.maidens = 0
		self.wickets = 0
		self.noballs = 0
		self.wides = 0

	def set_overs(self, overs):
		self.overs = overs
		return self

	def set_balls(self, balls):
		self.balls = balls
		return self

	def set_runs(self, runs):
		self.runs = runs
		return self

	def set_maidens(self, maidens):
		self.maidens = maidens
		return self

	def set_wickets(self, wickets):
		self.wickets = wickets
		return self

	def set_noballs(self, noballs):
		self.noballs = noballs
		return self
	
	def set_wides(self, wides):
		self.wides = wides
		return self

class Dismissal_Details:
	def __init__(self, dismissal_type, bowler, fielder, is_wicketkeeper):
		self.dismissal_type = dismissal_type
		self.bowler = bowler
		self.fielder = fielder
		self.is_wicketkeeper = is_wicketkeeper
	

