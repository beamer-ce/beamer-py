import redis 
import time
from Match import Match
from loguru import logger
import os

pid = os.getpid()
r = redis.Redis(host="localhost", port=6379, db=0, decode_responses= True)
logger.remove()
logger.add("logs/workers/w-%s.log" % (pid), rotation="1 Day", colorize=True, compression="zip")
logger.info("Starting worker process - %s" % (pid))

while(True):
    #logger.info("Ping.")
    task = r.brpop('match_queue', 5)
    if task is not None:
        logger.info("Got match id from queue: " + task[1])
        match = Match(None, None, None, None)
        match.simulate2(task[1])
        logger.info("Match id processed: " + task[1])
