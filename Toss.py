import random
from Constants import Play_Type
class Toss:
	def __init__(self, team1_id: int, team2_id:int, toss_winner: int = None, toss_decision: int = None):
		self.team1_id: int = team1_id
		self.team2_id: int = team2_id
		self.toss_winner: int = toss_winner
		self.toss_decision: str = toss_decision
	
	def toss(self):
		if(random.randint(1, 10) < 5):
			self.toss_winner = self.team1_id
		else:
			self.toss_winner = self.team2_id
		if(random.randint(1, 10) < 5):
			self.toss_decision = Play_Type.BAT
		else:
			self.toss_decision = Play_Type.BOWL
	
	def __str__(self):
		return "Toss [team1: {}, team2: {}, winner: {}, decision: {}]".format(self.team1_id, self.team2_id, self.toss_winner, self.toss_decision)