from dal.TeamDAL import TeamDAL
from Player import Player
from Team import Team
from Ground import Ground
from Match import Match
from loguru import logger
from config.Config import Config
from Machine import OutcomeCalculator
from dal.MachineDAL import MachineDAL
from utils.DatabaseUtils import Database
import pprint
import sqlite3
from config.Config import SL_Database_Config

def main():
	pp = pprint.PrettyPrinter(indent=4)
	logger.remove()
	logger.add(Config.ce_log_path, rotation="1 Day", colorize=True, compression="zip")
	# register_teams()
	

	team1 = Team.get_team_by_id(26)
	for p in team1.player_list:
		print(p)
	team2 = Team.get_team_by_id(27)
	for p in team2.player_list:
		print(p)
	ground = Ground.get_ground_by_id(2474)
	match = Match(team1, team2, "odi", ground)
	match.simulate()
	
	# players = Player.get_players_by_id([1407, 746], None)
	# # for player in players:
	# # 	print(player.name)
	# # 	for i in range(1, 51, 5):
	# # 		print(f"OVER: {i}")
	# # 		MachineDAL.get_bowling_p(player, i, "odi")
	# # 		print("~"*50)
	# # 	print("#"*50)
	# #pp.pprint(MachineDAL.get_bowling_p(players[0], 2, "odi"))
	# pp.pprint(MachineDAL.get_batting_p(players[0], 2, "odi"))
	# players = Player.get_players_by_id([1173, 1409, 1402], None)
	# #MachineDAL.get_batting_p(players[0], 41, "odi")
	# MachineDAL.get_bowler_f([players[0].ci_id, players[1].ci_id, players[2].ci_id], 3, "odi")
	# build_f_script()

def register_teams():
	afg = Player.get_players_by_id(None, [352048, 533956, 935553 , 524049, 320652, 25913, 318339, 793463, 516561, 524050, 974109])
	aus = Player.get_players_by_id(None, [219889, 5334, 267192, 334337, 326434 , 325012, 325026, 489889, 311592, 272477, 272279])
	ban = Player.get_players_by_id(None, [56194, 436677, 56143, 56029 , 536936, 56025, 550133, 629070, 629063, 56007 , 330902])
	eng = Player.get_players_by_id(None, [298438, 297433, 303669, 24598 , 311158, 308967 , 247235, 19264, 669855, 244497, 351588])
	ind = Player.get_players_by_id(None, [422108, 34102, 253802 , 931581, 30045, 625371, 28081 , 234675, 326016, 430246, 625383])
	nz = Player.get_players_by_id(None, [226492, 539511, 277906 , 38699, 388802 , 355269, 55395, 502714, 506612, 277912, 493773])
	pak = Player.get_players_by_id(None, [512191, 568276, 348144 , 41434, 318788, 227758, 227760 , 43590, 922943, 290948, 1072470])
	rsa = Player.get_players_by_id(None, [600498, 379143 , 44828 , 337790, 44932, 327830, 540316, 439952, 550215, 40618, 379145])
	sl = Player.get_players_by_id(None, [227772 , 300631 , 784369, 629074, 49764, 301236, 328026, 465793, 370040, 499594, 49758])
	wi = Player.get_players_by_id(None, [51880, 348054, 581379 , 670025, 604302, 391485 , 457249, 670013, 495551, 914567, 446101])

	TeamDAL.register_team(Team(None, "Afghanistan", afg))
	TeamDAL.register_team(Team(None, "Australia", aus))
	TeamDAL.register_team(Team(None, "Bangladesh", ban))
	TeamDAL.register_team(Team(None, "England", eng))
	TeamDAL.register_team(Team(None, "India", ind))
	TeamDAL.register_team(Team(None, "New Zealand", nz))
	TeamDAL.register_team(Team(None, "Pakistan", pak))
	TeamDAL.register_team(Team(None, "South Africa", rsa))
	TeamDAL.register_team(Team(None, "Sri Lanka", sl))
	TeamDAL.register_team(Team(None, "West Indies", wi))

	# players2 = Player.get_players_by_id([1407, 746, 872, 1592, 443, 1447, 475, 910, 1173, 1409, 1402], None)
	# team1 = Team(None, "New Zealand WC 2019", players1)
	# team2 = Team(None, "India WC 2019", players2)
	# TeamDAL.register_team(team1)
	# TeamDAL.register_team(team2)
	pass

def build_p_script():
	GET_PLAYERS = "SELECT id, ci_id from Players"
	SAVE_BATTING_P = """INSERT INTO batting_probability (batsman_ci_id, 
	batsman_id, over, p_0, p_1, p_2, p_3, p_4, p_6, p_out) VALUES 
	(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
	SAVE_BOWLING_P = """INSERT INTO bowling_probability (bowler_ci_id,
	bowler_id, over, p_great, p_good, p_ok, p_bad, p_wide, p_no_ball) VALUES 
	(?, ?, ?, ?, ?, ?, ?, ?, ?)"""
	read_connection = sqlite3.connect(SL_Database_Config.db_path)
	read_connection.row_factory = sqlite3.Row
	player_list = []
	with read_connection:
		cursor = read_connection.cursor()
		rows = cursor.execute(GET_PLAYERS)
		for row in rows:
			row = dict(row)
			player_list.append({'id': row['id'], 'ci_id': row['ci_id']})
	temp = 0
	for player in player_list:
		# if(temp >= 1):
		# 	break
		id = player['id']
		ci_id = player['ci_id']
		player = Player.get_players_by_id([id], None)[0]
		for over in range(0, 51):
			p_bat = MachineDAL.get_batting_p(player, over, "odi")
			p_bowl = MachineDAL.get_bowling_p(player, over, "odi")
			bat_p_0 = p_bat['outcome_array'][0]['p']
			bat_p_1 = p_bat['outcome_array'][1]['p']
			bat_p_2 = p_bat['outcome_array'][2]['p']
			bat_p_3 = p_bat['outcome_array'][3]['p']
			bat_p_4 = p_bat['outcome_array'][4]['p']
			bat_p_6 = p_bat['outcome_array'][5]['p']
			bat_p_out = p_bat['outcome_array'][6]['p']

			bowl_p_great = p_bowl['outcome_array'][0]['p']
			bowl_p_good = p_bowl['outcome_array'][1]['p']
			bowl_p_ok = p_bowl['outcome_array'][2]['p']
			bowl_p_bad = p_bowl['outcome_array'][3]['p']
			bowl_p_wide = p_bowl['outcome_array'][4]['p']
			bowl_p_no_ball = p_bowl['outcome_array'][5]['p']
			# print((ci_id, id, over, p_0, p_1, p_2, p_3, p_4, p_6, p_out))
			write_connection = sqlite3.connect(SL_Database_Config.db_path)
			with write_connection:
				cursor = write_connection.cursor()
				cursor.execute(SAVE_BATTING_P, (ci_id, id, over, bat_p_0, bat_p_1, bat_p_2, bat_p_3, bat_p_4, bat_p_6, bat_p_out))
				cursor.execute(SAVE_BOWLING_P, (ci_id, id, over, bowl_p_great, bowl_p_good, bowl_p_ok, bowl_p_bad, bowl_p_wide, bowl_p_no_ball))
			logger.info("Saved for ID: {}, Over: {}", id, over)
		temp += 1

def build_f_script():
	GET_PLAYERS = "SELECT id, ci_id FROM Players"
	SAVE_BOWLING_FREQ = """INSERT INTO bowler_frequency (bowler_id,
	bowler_ci_id, over, frequency, match_type) VALUES 
	(?, ?, ?, ?, ?)
	"""
	player_list = []
	read_conn = sqlite3.connect(SL_Database_Config.db_path)
	read_conn.row_factory  = sqlite3.Row
	with read_conn:
		cursor = read_conn.cursor()
		rows = cursor.execute(GET_PLAYERS)
		for row in rows:
			row = dict(row)
			player_list.append({'id': row['id'], 'ci_id': row['ci_id']})
	temp = 0
	for player in player_list:
		# if(temp > 1):
		# 	break
		id = player['id']
		ci_id = player['ci_id']
		for over in range(0, 51):
			bowl_f = MachineDAL.get_bowler_f([ci_id], over, "odi")
			write_connection = sqlite3.connect(SL_Database_Config.db_path)
			with write_connection:
				cursor = write_connection.cursor()
				cursor.execute(SAVE_BOWLING_FREQ, (id, ci_id, over, bowl_f[ci_id], "odi"))
			logger.info("Saved for ID: {}, Over: {}", id, over)
		temp+=1

if __name__ == "__main__":
	main()