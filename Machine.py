"""Core of the engine. Calculates the probability of an outcome based on prior data. Spits out
an outcome based on that probability.
"""
# `Intentions can be a fickle business.` -- Harold Finch
#from Inning import Inning
from Constants import Batting_Outcome, Bowling_Outcome, Player_Type
from Constants import Dismissal_Type, Match_Type
from random import randint
from typing import Dict
from dal.MachineDAL import MachineDAL
from modifiers.Batting_Outcome_Modifiers import Batting_Outcome_Modifier
from modifiers.Bowling_Outcome_Modifiers import Bowling_Outcome_Modifiers
from copy import deepcopy
from Constants import BOWLER_TYPE
class OutcomeCalculator:
	@staticmethod
	def get_batting_outcome(inning, bowling_outcome: str) -> Dict:
		outcome_list = [0, 1, 2, 3, 4, 6, Batting_Outcome.OUT]
		#outcome_list = [0, 1, 2, 3, 4, 6, 0]
		dismissal_list = \
			[Dismissal_Type.CAUGHT, 
			Dismissal_Type.RUNOUT, 
			Dismissal_Type.BOWLED, 
			Dismissal_Type.STUMPED, 
			Dismissal_Type.LEG_BEFORE_WICKET]
		outcome = OutcomeCalculator.get_batting_p(inning, bowling_outcome, inning.on_strike, inning.current_over, inning.match_type)
		if(outcome != Batting_Outcome.OUT):
			#If the ball bowled is a wide, then hitting a six on it is impossible
			if(bowling_outcome == Bowling_Outcome.WIDE and outcome == 6):
				return (Batting_Outcome.RUNS, 0)	
			return (Batting_Outcome.RUNS, outcome)
		elif(outcome == Batting_Outcome.OUT):
			#If the ball bowled is a wide, then the only way to get out is 
			#either runout or stumped.
			return (Batting_Outcome.OUT, OutcomeCalculator.get_dismissal_type(inning, bowling_outcome, inning.current_bowler))

			# if(bowling_outcome == Bowling_Outcome.WIDE):
			# 	if(randint(1, 10) < 5):
			# 		return (Batting_Outcome.OUT, dismissal_list[1])
			# 	else:
			# 		return (Batting_Outcome.OUT, dismissal_list[3])
			
			# #Else: proceed with the normal flow.
			# wicket_type = randint(0, 4)
			# return (Batting_Outcome.OUT, dismissal_list[wicket_type])
	
	@staticmethod
	def get_bowling_outcome(inning) -> str:
		outcome_list = \
			[Bowling_Outcome.GREAT,
			Bowling_Outcome.GOOD,
			Bowling_Outcome.OK,
			Bowling_Outcome.BAD,
			Bowling_Outcome.WIDE,
			Bowling_Outcome.NO_BALL]
		outcome = OutcomeCalculator.get_bowling_p(inning, inning.current_bowler, inning.current_over, inning.match_type)
		return outcome

	@staticmethod
	def get_bowling_p(inning, bowler, over, match_type):
		#bowling_map = MachineDAL.get_bowling_p(bowler, over, match_type)
		bowling_map = deepcopy(bowler.bowling_outcome_map)
		modified = Bowling_Outcome_Modifiers(inning, bowling_map)
		bowling_map = modified.process().get()
		result = randint(0, bowling_map['total_ball_count'])
		for ball_type in bowling_map['outcome_array']:
			if(result <= ball_type['acc']):
				return ball_type['ball_type']

	@staticmethod
	def get_batting_p(inning, bowling_outcome, batsman, over, match_type):
		#batting_map = MachineDAL.get_batting_p(batsman, over, match_type)
		batting_map = deepcopy(batsman.batting_outcome_map)
		modified = Batting_Outcome_Modifier(inning, batting_map, bowling_outcome)
		batting_map = modified.process().get()
		result = randint(0, batting_map['total_ball_count'])
		for bat_type in batting_map['outcome_array']:
			if(result <= bat_type['acc']):
				return bat_type['bat_type']

	@staticmethod
	def get_dismissal_type(inn, bowling_outcome, bowler):
		dismissal_list = \
			[Dismissal_Type.CAUGHT, 
			Dismissal_Type.RUNOUT, 
			Dismissal_Type.BOWLED, 
			Dismissal_Type.STUMPED, 
			Dismissal_Type.LEG_BEFORE_WICKET]

		if(bowling_outcome == Bowling_Outcome.WIDE):
			return dismissal_list[1]

		if (bowler.bowling_style in BOWLER_TYPE.SLOW):
			freq_map = [
				{ 'type': Dismissal_Type.BOWLED, 'freq': 6367, 'acc': 6367 },
				{ 'type': Dismissal_Type.CAUGHT, 'freq': 20418, 'acc': 26785 },
				{ 'type': Dismissal_Type.RUNOUT, 'freq': 2081, 'acc': 28866 },
				{ 'type': Dismissal_Type.STUMPED, 'freq': 823, 'acc': 29689 },
				{ 'type': Dismissal_Type.LEG_BEFORE_WICKET, 'freq': 3716, 'acc': 33405 }
			]
			total_acc = 33405
			random_number = randint(0, total_acc)
			for d_t in freq_map:
				if (random_number <= d_t['acc']):
					#print("Returning: %s (%s): %s" % (bowler.fielding_name, bowler.bowling_style, d_t['type']))
					return d_t['type']
		else:
			freq_map = [
				{ 'type': Dismissal_Type.BOWLED, 'freq': 6367, 'acc': 6367 },
				{ 'type': Dismissal_Type.CAUGHT, 'freq': 20418, 'acc': 26785 },
				{ 'type': Dismissal_Type.RUNOUT, 'freq': 2081, 'acc': 28866 },
				{ 'type': Dismissal_Type.LEG_BEFORE_WICKET, 'freq': 3716, 'acc': 32582 }
			]
			total_acc = 32582
			random_number = randint(0, total_acc)
			for d_t in freq_map:
				if (random_number <= d_t['acc']):
					#print("Returning: %s (%s): %s" % (bowler.fielding_name, bowler.bowling_style, d_t['type']))
					return d_t['type']

class BowlerSelector:
	@staticmethod
	def get_next_bowler(inn, prev_bowler, over, match_type):
		"""Returns the next bowler to bowl the over based on the existing data
		"""
		#Minimum number of bowlers required to bowl all the 20/50 overs in odi/t20 games.
		min_bowlers_requied = 6
		bowler_list = []
		for player in inn.bowling_team.player_list:
			if (player.player_type == Player_Type.BOWLER or player.player_type == Player_Type.ALLROUNDER):
				bowler_list.append(player)
		
		#if the number of bowlers is less then minimum required, pick some random players
		#and hand them the ball :D
		if(len(bowler_list) < min_bowlers_requied):
			for i in range(0, (min_bowlers_requied - len(bowler_list))):
				random_bowler = inn.bowling_team.get_random_bowler(bowler_list)
				bowler_list.append(random_bowler)
		
		#for each bowler, get the number of balls bowled in the current over + 1
		bowler_ci_id_list = []
		for bowler in bowler_list:
			bowler_ci_id_list.append(bowler.ci_id)

		ball_freq = MachineDAL.get_bowler_f(bowler_ci_id_list, over, Match_Type.ODI)
		# print("@"*100)
		# print(ball_freq)
		# print("@"*100)
		
		bowler_ci_id_priority_list = (sorted(ball_freq, key = ball_freq.get))[::-1]
		bowler_priority_list = []

		#get a list of sorted bowlers in decreasing priority (sorted in descending by ball frequency)
		for bowler_ci_id in bowler_ci_id_priority_list:
			for b in bowler_list:
				if b.ci_id == bowler_ci_id:
					bowler_priority_list.append(b)

		if(prev_bowler == None):
			return bowler_priority_list[0]
		
		# for index, b in enumerate(bowler_priority_list):
		# 	if(over > 20 and over < 40):
		# 		if(b.overs_bowled < 5):
		# 			bowler_priority_list.insert(0, b)
		# 			del bowler_priority_list[index]

		# for b in bowler_priority_list:
		# 	print(f"BOWLER: {b.name} | OVERS BOWLED: {b.overs_bowled}")

		for b in bowler_priority_list:
			if(b in inn.bowling_team.opening_bowlers):
				result = BowlerSelector.can_bowl(inn, b, over)
				if(result == False):
					continue
			if(b.id != prev_bowler.id and b.overs_bowled < Match_Type.MAX_OVERS_PER_BOWLER[match_type]):
				if(over < 10 and len(inn.bowling_team.opening_bowlers) < 2):
					inn.bowling_team.opening_bowlers.append(b)
				return b

	

	@staticmethod
	def can_bowl(inn, bowler, over):
		""" This method makes sure that the opening bowlers don't spend their all 
		their overs before the crucial last 5 overs.
		"""
		if(over < 10 or over > 45):
			return True
		total_overs_left = 0
		for b in inn.bowling_team.opening_bowlers:
			total_overs_left += 10 - b.overs_bowled
		if(total_overs_left > 5):
			return True
		else:
			return False