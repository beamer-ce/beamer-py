from utils.DatabaseUtils import Database
import psycopg2
import psycopg2.extras
from loguru import logger

class TeamDAL:
	""" Data access layer for the the Team model.
	"""
	@staticmethod
	def get_team_summary():
		GET_TEAM_SUMMARY = """SELECT id, name, shortform FROM team_info"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			cursor = connection.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
			cursor.execute(GET_TEAM_SUMMARY)
			rows = cursor.fetchall()
			return rows
		except (Exception, psycopg2.DatabaseError) as e:
			#Rollback due to error
			logger.error("Error while fetch team info.")
			logger.error("{}", e)
			return []
		finally:
			#Clean up resources
			if cursor:
				cursor.close()
			if connection:
				connection.close()

	@staticmethod
	def get_team_by_id(id):
		""" Fetch a team from the database using the id.
		
		Returns:
			(RealDictRow) -- ArrayDict if data is found.
			[] -- if no data is found.
		"""
		
		GET_TEAM = """SELECT ti.id, ti.name, ti.shortform, td.player_id, td.player_role, td.position FROM team_info ti JOIN team_data td ON ti.id = td.team_id 
		WHERE ti.id = %s"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			cursor = connection.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
			cursor.execute(GET_TEAM, (id, ))
			rows = cursor.fetchall()
			return rows
		except (Exception, psycopg2.DatabaseError) as e:
			#Rollback due to error
			logger.error("Error while fetch team info.")
			logger.error("{}", e)
			return []
		finally:
			#Clean up resources
			if cursor:
				cursor.close()
			if connection:
				connection.close()

	@staticmethod
	def register_team(team):
		""" Inserts the team info in database.
		Arguments:
			team: Team
		
		Returns:
			int: Id of the newly inserted team.
		"""
		REGISTER_TEAM = """INSERT INTO team_info (name) VALUES (%s) RETURNING id """
		ADD_PLAYER = """INSERT INTO team_data (team_id, player_id, position) VALUES (%s, %s, %s)"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			#Setting autocommit false for transactions, if anything goes wrong, rollback the transaction
			connection.autocommit = False
			cursor = connection.cursor()
			cursor.execute(REGISTER_TEAM, (team.team_name, ))
			new_team_id = cursor.fetchone()[0]
			player_rows = []
			for index, player in enumerate(team.player_list):	
				player_rows.append((new_team_id, player.id, index))
			
			cursor.executemany(ADD_PLAYER, player_rows)
			connection.commit()
			logger.info("Registered new team: ID: {}, Name: {}", new_team_id, team.team_name)
			return new_team_id

		except (Exception, psycopg2.DatabaseError) as e:
			#Rollback due to error
			logger.error("Error while registering new team.")
			logger.error("{}", e)
			connection.rollback()
			return None
		finally:
			#Clean up resources
			if cursor:
				cursor.close()
			if connection:
				connection.close()