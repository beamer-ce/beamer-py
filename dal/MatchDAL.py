from utils.DatabaseUtils import Database
import psycopg2
import psycopg2.extras
from loguru import logger

class MatchDAL:
	""" Data access layer for the Match model
	"""
	@staticmethod
	def register_match(match):
		""" Inserts the match info in database.
		Arguments:
			`match`: Match
		
		Returns:
			`int`: Id of the newly inserted match.
		"""
		REGISTER_MATCH = """INSERT INTO match_info (team1_id, team2_id, 
		match_type, ground_id, toss_winner, toss_decision, finished) VALUES (%s, %s, %s, %s, %s, %s, 0) RETURNING id"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			cursor = connection.cursor()
			cursor.execute(REGISTER_MATCH, (match.team1.team_id, match.team2.team_id, match.match_type, match.ground.id, match.toss.toss_winner, match.toss.toss_decision))
			new_match_id = cursor.fetchone()[0]
			connection.commit()
			logger.info("Registered new match: ID: {}", new_match_id)
			return new_match_id
		except (Exception, psycopg2.DatabaseError) as e:
			logger.error("Error while registering new match.")
			logger.error("{}", e)
			return None
		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()
	
	@staticmethod
	def finish_match(id):
		FINISH_MATCH = """UPDATE match_info SET finished = 1 WHERE id = %s"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			cursor = connection.cursor()
			cursor.execute(FINISH_MATCH, (id, ))
			connection.commit()
			logger.info("Finished match: ID: {}", id)
		except (Exception, psycopg2.DatabaseError) as e:
			logger.error("Error while finishing match.")
			logger.error("{}", e)
			return None
		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()

	@staticmethod
	def save_match(inning):
		""" Inserts the match data in database.
		Arguments:
			`match`: Match
		"""
		SAVE_INNING = """INSERT INTO match_data (match_id, batting_team, bowling_team, inning, 
		bowler, on_strike, runner_end, ball_count, bowling_outcome, batting_outcome, outcome_desc, dismissal_type, 
		dismissed_batsman, fielder, is_wicketkeeper, is_wicket, commentary) VALUES 
		(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			connection.autocommit = False
			cursor = connection.cursor()
			cursor.executemany(SAVE_INNING, inning)
			connection.commit()
		except (Exception, psycopg2.DatabaseError) as e:
			logger.error("Error while saving match data.")
			logger.error("{}", e)
			connection.rollback()
			return None
		finally:
			#Clean up resources
			if cursor:
				cursor.close()
			if connection:
				connection.close()

	@staticmethod
	def get_match_data(match_id, inn):
		GET_MATCH_DATA = """ SELECT match_id, batting_team, bowling_team, inning, bowler,
		on_strike, runner_end, ball_count, bowling_outcome, batting_outcome, outcome_desc, 
		dismissal_type, dismissed_batsman, fielder, is_wicketkeeper, is_wicket, commentary FROM
		match_data WHERE match_id = %s and inning = %s ORDER BY ball_count
		"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			cursor = connection.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
			cursor.execute(GET_MATCH_DATA, (match_id, inn))
			rows = cursor.fetchall()
			return rows
		except (Exception, psycopg2.DatabaseError) as e:
			logger.error("Error while retrieving match data.")
			logger.error("{}", e)
			return None
		finally:
			#Clean up resources
			if cursor:
				cursor.close()
			if connection:
				connection.close()

	@staticmethod
	def get_match_info(match_id):
		GET_MATCH_INFO = """ SELECT id, team1_id, team2_id, match_type, ground_id, toss_winner, 
		toss_decision, match_result, finished, processing FROM match_info WHERE id = %s
		"""
		connection = None
		cursor = None
		try:
			connection = Database.pg_connection()
			cursor = connection.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
			cursor.execute(GET_MATCH_INFO, (match_id, ))
			row = cursor.fetchone()
			return row
		except (Exception, psycopg2.DatabaseError) as e:
			logger.error("Error while retrieving match info.")
			logger.error("{}", e)
			return None
		finally:
			#Clean up resources
			if cursor:
				cursor.close()
			if connection:
				connection.close()
