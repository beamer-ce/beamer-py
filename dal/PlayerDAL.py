from utils.DatabaseUtils import Database
from loguru import logger
class PlayerDAL:

	#Returns player data based on either id or ci_id
	@staticmethod
	def get_players_by_id(id_list = None, ci_id_list = None):
		clause = {
			'colName': None,
			'colValue': None
		}
		if(id_list is not None):
			clause['colName'] = "id"
			clause['colValue'] = id_list
		elif (ci_id_list is not None):
			clause['colName'] = "ci_id"
			clause['colValue'] = ci_id_list
		
		GET_PLAYERS = """SELECT id, ci_id, name, country, player_type, 
			batting_style, bowling_style, batting_name, fielding_name FROM players WHERE (%s) in (%s)""" % (clause['colName'], ','.join('?'*len(clause['colValue'])))
		
		connection = None
		cursor = None
		try:
			connection = Database.sl_connection()
			cursor = connection.cursor()
			cursor.execute(GET_PLAYERS, clause['colValue'])
			return cursor.fetchall()
		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()
		return None