from utils.DatabaseUtils import Database
from loguru import logger

class GroundDAL:
	""" Data access layer for the Ground (Stadium) model.
	"""
	@staticmethod
	def get_ground_by_id(id):
		if(id is None):
			logger.error("Null passed as value.")
			return None
		GET_GROUND = "SELECT id, ci_id, name, country FROM stadiums WHERE id = ?"
		connection = None
		cursor = None
		try:
			connection = Database.sl_connection()
			cursor = connection.cursor()
			cursor.execute(GET_GROUND, (id, ))
			return cursor.fetchone()
		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()
		return None

	@staticmethod
	def get_all_grounds():
		GET_GROUND = "SELECT id, ci_id, name, country FROM stadiums"
		connection = None
		cursor = None
		try:
			connection = Database.sl_connection()
			cursor = connection.cursor()
			cursor.execute(GET_GROUND)
			rows =  cursor.fetchall()
			return rows
		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()
		return None