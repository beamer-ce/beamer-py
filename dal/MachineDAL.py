from utils.DatabaseUtils import Database
from Constants import Match_Type
from Constants import Bowling_Outcome, Batting_Outcome
from config.Config import Config
import math
class MachineDAL:
	@staticmethod
	def get_bowling_p(bowler, over, match_type):
		"""Gets the total number of balls bowled by a player in a given period.
		Also returns the number of balls bowled of quality <X>.
		"""
		GET_BOWLING_P = "SELECT * FROM bowling_probability WHERE bowler_id = ? AND over = ? AND match_type = ?"
		span = 0
		connection = None
		cursor = None
		total_ball_count = 0

		try:
			connection = Database.sl_connection()
			cursor = connection.cursor()
			cursor.execute(GET_BOWLING_P,(bowler.id, over, match_type))
			row = cursor.fetchone()
			row = dict(row)

			bowling_outcome_array = [
				{ 'ball_type': Bowling_Outcome.GREAT, 'p': row['p_great'], 'acc': 0},
				{ 'ball_type': Bowling_Outcome.GOOD, 'p': row['p_good'], 'acc': 0},
				{ 'ball_type': Bowling_Outcome.OK, 'p': row['p_ok'], 'acc': 0},
				{ 'ball_type': Bowling_Outcome.BAD, 'p': row['p_bad'], 'acc': 0},
				{ 'ball_type': Bowling_Outcome.WIDE, 'p': row['p_wide'], 'acc': 0},
				{ 'ball_type': Bowling_Outcome.NO_BALL, 'p': row['p_no_ball'], 'acc': 0},
			]

			total_acc = 0
			for item in bowling_outcome_array:
				item['acc'] = total_acc + item['p']
				total_acc += item['p']

			return { 'outcome_array':bowling_outcome_array, 'total_ball_count' : total_acc, 'orig_ball_count': total_acc }

		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()

	@staticmethod
	def get_batting_p(batsman, over, match_type):
		GET_BATTING_P = "SELECT * FROM batting_probability WHERE batsman_id = ? AND over = ? AND match_type = ?"
		span = 0
		connection = None
		cursor = None
		total_ball_count = 0
	
		try:
			connection = Database.sl_connection()
			cursor = connection.cursor()
			cursor.execute(GET_BATTING_P, (batsman.id, over, match_type))
			row = cursor.fetchone()
			row = dict(row)

			batting_outcome_array = [
				{ 'bat_type': 0, 'p': row['p_0'], 'acc': 0},
				{ 'bat_type': 1, 'p': row['p_1'], 'acc': 0},
				{ 'bat_type': 2, 'p': row['p_2'], 'acc': 0},
				{ 'bat_type': 3, 'p': row['p_3'], 'acc': 0},
				{ 'bat_type': 4, 'p': row['p_4'], 'acc': 0},
				{ 'bat_type': 6, 'p': row['p_6'], 'acc': 0},
				{ 'bat_type': Batting_Outcome.OUT, 'p': row['p_out'], 'acc': 0},
			]

			total_acc = 0
			for item in batting_outcome_array:
				item['acc'] = total_acc + item['p']
				total_acc += item['p']

			return { 'outcome_array': batting_outcome_array, 'total_ball_count': total_acc, 'orig_ball_count': total_acc }
			
		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()

	@staticmethod
	def get_bowler_f(bowler_list, over, match_type):
		GET_BOWLING_F = """SELECT bowler_id, bowler_ci_id, over, frequency
		FROM bowler_frequency WHERE bowler_ci_id in (%s) AND over = ? AND match_type = ?
		""" % (','.join('?' * len(bowler_list)))
		connection = None
		cursor = None
		freq_map = {}
		try:
			connection = Database.sl_connection()
			cursor = connection.cursor()
			
			bowler_list.append(over)
			bowler_list.append(match_type)

			cursor.execute(GET_BOWLING_F, bowler_list)
			rows = cursor.fetchall()
			for row in rows:
				row = dict(row)
				freq_map[row['bowler_ci_id']] = row['frequency']
			
			bowler_list = bowler_list[:-2]

			return freq_map
		finally:
			if cursor:
				cursor.close()
			if connection:
				connection.close()

			
