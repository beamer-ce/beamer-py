from Constants import Bowling_Outcome, Batting_Outcome, Match_Type
from Constants import P_Mod
class Batting_Outcome_Modifier:
	def __init__(self, inn, batting_map, ball_quality):
		self.batting_map = batting_map
		self.ball_quality = ball_quality
		self.inn = inn
		self.sl_ball = [5]
		temp = 5
		while(temp < 300):
			temp += 6
			self.sl_ball.append(temp)

	
	def bowling_outcome_quality(self):
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		#print(f"(LC {lc_amt} | MC {mc_amt}) | SC {sc_amt}")
		if(self.ball_quality == Bowling_Outcome.GREAT):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += lc_amt * 2
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 3
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += sc_amt
			return self
		elif(self.ball_quality == Bowling_Outcome.GOOD):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 0):
					outcome['p'] += mc_amt * 2
				if(outcome['bat_type'] == 1):
					outcome['p'] += sc_amt * 2
				if(outcome['bat_type'] == 2):
					outcome['p'] += sc_amt
			return self
		elif(self.ball_quality == Bowling_Outcome.OK):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += sc_amt 
				if(outcome['bat_type'] == 1):
					outcome['p'] += sc_amt * 2
				if(outcome['bat_type'] == 4):
					outcome['p'] += sc_amt * 2
			return self
		elif(self.ball_quality == Bowling_Outcome.BAD):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 2):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += lc_amt
			return self
		elif(self.ball_quality == Bowling_Outcome.WIDE):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 100
				if(outcome['bat_type'] == 1):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] = 0 			#no six on wide balls :)
			return self
		elif(self.ball_quality == Bowling_Outcome.NO_BALL):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 1):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += lc_amt * 2
			return self
		return self

	def overs_left(self):
		overs_left = 50
		perc_overs_left = 100
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		#print(f"(LC {lc_amt} | MC {mc_amt}) | SC {sc_amt}")
		if(self.inn.match_type == Match_Type.ODI):
			overs_left = Match_Type.MAX_OVERS[Match_Type.ODI] - self.inn.current_over
			perc_overs_left = int((overs_left/Match_Type.MAX_OVERS[Match_Type.ODI]) * 100)
		elif(self.inn.match_type == Match_Type.T20):
			overs_left = Match_Type.MAX_OVERS[Match_Type.ODI] - self.inn.current_over
			perc_overs_left = int((overs_left/Match_Type.MAX_OVERS[Match_Type.T20]) * 100)
		
		if(perc_overs_left > 80):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 2
				if(outcome['bat_type'] == 2):
					outcome['p'] += lc_amt * 2
				if(outcome['bat_type'] == 4):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += sc_amt
			return self
		elif(perc_overs_left <= 80 and perc_overs_left > 60):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 2
				if(outcome['bat_type'] == 2):
					outcome['p'] += lc_amt * 2
				if(outcome['bat_type'] == 4):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += sc_amt
			return self
		elif(perc_overs_left <= 60 and perc_overs_left > 40):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += sc_amt
			return self
		elif(perc_overs_left <= 40 and perc_overs_left > 20):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += mc_amt
			return self
		elif(perc_overs_left <= 20):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += lc_amt
			return self
		return self
		
	def batsman_confidence(self):
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		#print(f"(LC {lc_amt} | MC {mc_amt}) | SC {sc_amt}")
		batsman_conf = self.inn.on_strike.batting_confidence
		if(batsman_conf < -0.5):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 1):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += sc_amt
			return self
		elif(batsman_conf >= -0.5 and batsman_conf < 0):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += sc_amt
			return self
		elif(batsman_conf >= 0 and batsman_conf < 0.5):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += sc_amt
			return self
		elif(batsman_conf >= 0.5):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 2):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += mc_amt
			return self
		return self

	def req_runrate(self):
		#no-op in first innings
		if(self.inn.inning == 1):
			return self
		overs_left = 50
		wickets_left = 10 - self.inn.current_wickets
		if(self.inn.match_type == Match_Type.ODI):
			overs_left = Match_Type.MAX_OVERS[Match_Type.ODI] - self.inn.current_over
		elif(self.inn.match_type == Match_Type.T20):
			overs_left = Match_Type.MAX_OVERS[Match_Type.ODI] - self.inn.current_over
		req_rr = ((self.inn.target - self.inn.current_score)/float(overs_left))
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		
		if(req_rr < 4):
			# Hack for preventing batsmen from hitting big shots when chasing
			# during a collapse.
			if(wickets_left < 5):
				for outcome in self.batting_map['outcome_array']:
					if(outcome['bat_type'] == 0):
						outcome['p'] += lc_amt * 5
					if(outcome['bat_type'] == 1):
						outcome['p'] += lc_amt * 5
			
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += lc_amt
			return self
		elif(req_rr >= 4 and req_rr < 6):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += mc_amt
			return self
		elif(req_rr >= 6 and req_rr < 10):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 2):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += mc_amt
			return self
		elif(req_rr >= 10):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += mc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += lc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += lc_amt
			return self
		return self

	def wickets_remaining(self):
		wickets_left = 10 - self.inn.current_wickets
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		
		#print(f"(LC {lc_amt} | MC {mc_amt}) | SC {sc_amt}")
		if(wickets_left > 8):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += sc_amt
			return self
		elif(wickets_left <= 8 and wickets_left > 6):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 4):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 6):
					outcome['p'] += sc_amt
			return self
		elif(wickets_left <= 6 and wickets_left > 3):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += sc_amt
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 4
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 4
				if(outcome['bat_type'] == 2):
					outcome['p'] += lc_amt * 4
				if(outcome['bat_type'] == 4):
					outcome['p'] += mc_amt
			return self
		elif(wickets_left <= 3):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 4
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 4
				if(outcome['bat_type'] == 2):
					outcome['p'] += lc_amt * 4
			return self
		return self
	
	def strike_rotate(self):
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		on_strike = self.inn.on_strike
		runner_end = self.inn.runner_end
		if(on_strike.player_type in ['batsman', 'allrounder'] and runner_end.player_type == 'bowler'):
			if(self.inn.current_ball in self.sl_ball):
				for outcome in self.batting_map['outcome_array']:
					if(outcome['bat_type'] == 0):
						outcome['p'] -= lc_amt * 2
					if(outcome['bat_type'] == 1):
						outcome['p'] += lc_amt * 2
					if(outcome['bat_type'] == 3):
						outcome['p'] += lc_amt * 2
				return self
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 2
				if(outcome['bat_type'] == 1):
					outcome['p'] -= lc_amt * 2
				if(outcome['bat_type'] == 3):
					outcome['p'] -= lc_amt * 2
			return self
		return self

	#bat defensively if wickets have fallen in the last 3 overs
	def defensive_bat(self):
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		wicket_count = self.get_total_wickets()
		if(wicket_count == 1):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 3
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 3
			return self
		elif(wicket_count == 2):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 4
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 4
			return self
		elif(wicket_count >= 3):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 5
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 5
			return self
		return self
	
	def bowler_on_strike(self):
		lc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.batting_map['orig_ball_count'] * (P_Mod.SC/100))
		on_strike = self.inn.on_strike
		if(on_strike.player_type == 'bowler'):
			for outcome in self.batting_map['outcome_array']:
				if(outcome['bat_type'] == 0):
					outcome['p'] += lc_amt * 3
				if(outcome['bat_type'] == 1):
					outcome['p'] += lc_amt * 3
				if(outcome['bat_type'] == Batting_Outcome.OUT):
					outcome['p'] += lc_amt * 5
		return self

	def process(self):
		self.bowling_outcome_quality().overs_left().batsman_confidence().req_runrate().wickets_remaining().strike_rotate().defensive_bat().bowler_on_strike()
		return self

	def get(self):
		total_acc = 0
		for outcome in self.batting_map['outcome_array']:
			outcome['acc'] = total_acc + outcome['p']
			total_acc += outcome['p']
		
		self.batting_map['total_ball_count'] = total_acc
		return self.batting_map

	def bowler_modifier(self, player):
		if(player.player_type == 'bowler'):
			return 3
		else:
			return 1
	
	def get_total_wickets(self):
		wicket_count = 0
		ball_data = self.inn.inning_data[-1:-20:-1]
		for ball in ball_data:
			if (ball.is_wicket):
				wicket_count += 1
		return wicket_count