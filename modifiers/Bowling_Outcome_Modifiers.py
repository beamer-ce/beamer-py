""" Contains the various modifiers tha process the outcome data for
the bowling outcome and modify is based on different conditions.
"""
from Constants import Bowling_Outcome, Batting_Outcome
from Constants import P_Mod
class Bowling_Outcome_Modifiers:
	def __init__(self, inn, bowling_map):
		self.bowling_map = bowling_map
		self.inn = inn
	
	def last_ball_outcome(self):
		lc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.SC/100))
		last_ball_bowled = None
		for ball in self.inn.inning_data:
			if (ball.bowler.id == self.inn.current_bowler.id):
				last_ball_bowled = ball
				break

		if(last_ball_bowled == None):
			return self

		if(ball.bowling_outcome == Bowling_Outcome.NO_BALL):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.BAD):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.WIDE):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.NO_BALL):
					outcome['p'] += mc_amt
			return self
		
		if(ball.batting_outcome == Batting_Outcome.OUT):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.GREAT):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.GOOD):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.OK):
					outcome['p'] += mc_amt
			return self
		return self
	
	def bowler_confidence(self):
		lc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.SC/100))
		bowling_conf = self.inn.current_bowler.bowling_confidence 
		if(bowling_conf < -0.5):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.BAD):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.WIDE):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.NO_BALL):
					outcome['p'] += mc_amt
			return self
		elif(bowling_conf >= -0.5 and bowling_conf < 0):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.BAD):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.WIDE):
					outcome['p'] += mc_amt
				if(outcome['ball_type'] == Bowling_Outcome.NO_BALL):
					outcome['p'] += mc_amt
			return self
		elif(bowling_conf >= 0 and bowling_conf < 0.5):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.GREAT):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.GOOD):
					outcome['p'] += mc_amt
				if(outcome['ball_type'] == Bowling_Outcome.OK):
					outcome['p'] += mc_amt
			return self
		elif(bowling_conf >= 0.5):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.GREAT):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.GOOD):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.OK):
					outcome['p'] += mc_amt
			return self
		else:
			return self

	def current_runrate(self):
		lc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.LC/100))
		mc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.MC/100))
		sc_amt = round(self.bowling_map['orig_ball_count'] * (P_Mod.SC/100))
		if(self.inn.current_over == 0):
			return self
		runrate = self.inn.current_score / (self.inn.current_over)
		if(runrate >= 0 and runrate < 4):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.GREAT):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.GOOD):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.OK):
					outcome['p'] += mc_amt
			return self
		elif(runrate >= 4 and runrate < 6):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.GREAT):
					outcome['p'] += mc_amt
				if(outcome['ball_type'] == Bowling_Outcome.GOOD):
					outcome['p'] += mc_amt
				if(outcome['ball_type'] == Bowling_Outcome.OK):
					outcome['p'] += lc_amt
			return self
		elif(runrate >= 6 and runrate < 10):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.BAD):
					outcome['p'] += mc_amt
				if(outcome['ball_type'] == Bowling_Outcome.WIDE):
					outcome['p'] += sc_amt
				if(outcome['ball_type'] == Bowling_Outcome.NO_BALL):
					outcome['p'] += sc_amt
			return self
		elif(runrate >= 10):
			for outcome in self.bowling_map['outcome_array']:
				if(outcome['ball_type'] == Bowling_Outcome.BAD):
					outcome['p'] += lc_amt
				if(outcome['ball_type'] == Bowling_Outcome.WIDE):
					outcome['p'] += sc_amt
				if(outcome['ball_type'] == Bowling_Outcome.NO_BALL):
					outcome['p'] += sc_amt
			return self
		else:
			return self
	
	def process(self):
		self.last_ball_outcome().bowler_confidence().current_runrate()
		return self
	
	def get(self):
		total_acc = 0
		for outcome in self.bowling_map['outcome_array']:
			outcome['acc'] = total_acc + outcome['p']
			total_acc += outcome['p']
		
		self.bowling_map['total_ball_count'] = total_acc
		return self.bowling_map